/*
 * Creates an intro_graph.json file using a WDM XML input file
 */

const fs = require('fs');
const path = require('path');
const parser = require('xml2js').parseString;


const WDM_XML = path.join(__dirname, "..", "data", "intro_graph.xml");
const OUTPUT_GRAPH = path.join(__dirname, "..", "data", "intro_graph.json");


parser(fs.readFileSync(WDM_XML), (err, wdmXml) => {
	if(err) { throw err; }

	const introJson = {};

	wdmXml.osm.node
	.filter(w => w.$.action !== "delete")
	.forEach(n => {
		const njson = {
			id: `n${n.$.id}`,
			loc: [ parseFloat(n.$.lon), parseFloat(n.$.lat) ]
		};

		if(n.tag) {
			njson.tags = {};

			n.tag.forEach(t => {
				njson.tags[t.$.k] = t.$.v;
			});
		}

		introJson[njson.id] = njson;
	});

	wdmXml.osm.way
	.filter(w => w.$.action !== "delete")
	.forEach(w => {
		const wjson = {
			id: `w${w.$.id}`,
			nodes: w.nd.map(wnd => `n${wnd.$.ref}`)
		};

		if(w.tag) {
			wjson.tags = {};

			w.tag.forEach(t => {
				wjson.tags[t.$.k] = t.$.v;
			});
		}

		introJson[wjson.id] = wjson;
	});

	fs.writeFileSync(OUTPUT_GRAPH, JSON.stringify(introJson, null, 2));
});
