import { dispatch as d3_dispatch } from 'd3-dispatch';

import { presetManager } from '../../presets';
import { t } from '../../core/localizer';
import { modeBrowse } from '../../modes/browse';
import { modeSelect } from '../../modes/select';
import { utilArrayUniq, utilRebind } from '../../util';
import { helpHtml, icon, pad, pointBox, isMostlySquare, selectMenuItem, transitionTime } from './helper';


export function uiIntroTips(context, reveal) {
    var dispatch = d3_dispatch('done');
    var timeouts = [];

    var dblClickNodeRoadID = 'w-105899';
    var dblClickNodeRoaddEndID = 'n84';
    var dblClickNodeRoadAddNode = [2.6502537, 48.8383257];
    var midpointNodeRoadID = 'w35';
    var midpointNode = [2.6501932, 48.8384642];
    var midpointNodeDestination = [2.6502388, 48.8384717];


    var washingtonStreetID = 'w-105727';
    var twelfthAvenueID = 'w-105732';
    var eleventhAvenueEndID = 'n-138630';
    var twelfthAvenueEndID = 'n-138634';
    var _washingtonSegmentID = null;
    var eleventhAvenueEnd = context.entity(eleventhAvenueEndID).loc;
    var twelfthAvenueEnd = context.entity(twelfthAvenueEndID).loc;
    var deleteLinesLoc = [2.6493312, 48.8394951];
    var twelfthAvenue = [2.6492999, 48.8395168];

    var hospital = [2.6531741, 48.8384408];
    var hospitalID = 'w-105882';


    var chapter = {
        title: 'intro.tips.title'
    };


    function timeout(f, t) {
        timeouts.push(window.setTimeout(f, t));
    }


    function eventCancel(d3_event) {
        d3_event.stopPropagation();
        d3_event.preventDefault();
    }


    function updateLine() {
        context.enter(modeBrowse(context));
        context.history().reset('initial');

        var msec = transitionTime(dblClickNodeRoadAddNode, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(dblClickNodeRoadAddNode, 20, msec);

        timeout(function() {
            var padding = 250 * Math.pow(2, context.map().zoom() - 20);
            var box = pad(dblClickNodeRoadAddNode, padding, context);
            var advance = function() { continueTo(addNode); };

            reveal(box, helpHtml('intro.tips.update_line'),
                { buttonText: t.html('intro.ok'), buttonCallback: advance }
            );

            context.map().on('move.intro drawn.intro', function() {
                var padding = 250 * Math.pow(2, context.map().zoom() - 20);
                var box = pad(dblClickNodeRoadAddNode, padding, context);
                reveal(box, helpHtml('intro.tips.update_line'),
                    { duration: 0, buttonText: t.html('intro.ok'), buttonCallback: advance }
                );
            });
        }, msec + 100);

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            nextStep();
        }
    }


    function addNode() {
        if (!context.hasEntity(dblClickNodeRoadID) || !context.hasEntity(dblClickNodeRoaddEndID)) {
            return chapter.restart();
        }

        var msec = transitionTime(dblClickNodeRoadAddNode, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(dblClickNodeRoadAddNode, 21, msec);

        timeout(function() {
            var padding = 40 * Math.pow(2, context.map().zoom() - 20);
            var box = pad(dblClickNodeRoadAddNode, padding, context);
            var addNodeString = helpHtml('intro.tips.add_node' + (context.lastPointerType() === 'mouse' ? '' : '_touch'));
            reveal(box, addNodeString);

            context.map().on('move.intro drawn.intro', function() {
                var padding = 40 * Math.pow(2, context.map().zoom() - 20);
                var box = pad(dblClickNodeRoadAddNode, padding, context);
                reveal(box, addNodeString, { duration: 0 });
            });

            context.history().on('change.intro', function(changed) {
                if (!context.hasEntity(dblClickNodeRoadID) || !context.hasEntity(dblClickNodeRoaddEndID)) {
                    return continueTo(updateLine);
                }
                if (changed.created().length === 1) {
                    timeout(function() { continueTo(startDragEndpoint); }, 500);
                }
            });
        }, msec + 100);

        context.on('enter.intro', function(mode) {
            if (mode.id !== 'select') {
                continueTo(updateLine);
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.history().on('change.intro', null);
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function startDragEndpoint() {
        if (!context.hasEntity(dblClickNodeRoadID) || !context.hasEntity(dblClickNodeRoaddEndID)) {
            return continueTo(updateLine);
        }
        var padding = 80 * Math.pow(2, context.map().zoom() - 20);
        var box = pad(dblClickNodeRoadAddNode, padding, context);
        reveal(box, helpHtml('intro.tips.drag_to_pos'),
            { duration: 0, buttonText: t.html('intro.ok'), buttonCallback: startDragMidpoint }
        );

        context.map().on('move.intro drawn.intro', function() {
            if (!context.hasEntity(dblClickNodeRoadID) || !context.hasEntity(dblClickNodeRoaddEndID)) {
                return continueTo(updateLine);
            }
            var padding = 80 * Math.pow(2, context.map().zoom() - 20);
            var box = pad(dblClickNodeRoadAddNode, padding, context);
            //~ box.height += 400;
            reveal(box, helpHtml('intro.tips.drag_to_pos'),
                { duration: 0, buttonText: t.html('intro.ok'), buttonCallback: startDragMidpoint }
            );
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            nextStep();
        }
    }


    function startDragMidpoint() {
        if (!context.hasEntity(midpointNodeRoadID)) {
            return continueTo(updateLine);
        }
        if (context.selectedIDs().indexOf(midpointNodeRoadID) === -1) {
            context.enter(modeSelect(context, [midpointNodeRoadID]));
        }

        var msec = transitionTime(midpointNode, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(midpointNode, 20, msec);

        timeout(function() {
            var padding = 120 * Math.pow(2, context.map().zoom() - 20);
            var box = pad(midpointNode, padding, context);
            reveal(box, helpHtml('intro.tips.start_drag_midpoint'));

            context.map().on('move.intro drawn.intro', function() {
                if (!context.hasEntity(midpointNodeRoadID)) {
                    return continueTo(updateLine);
                }
                var padding = 120 * Math.pow(2, context.map().zoom() - 20);
                var box = pad(midpointNode, padding, context);
                reveal(box, helpHtml('intro.tips.start_drag_midpoint'), { duration: 0 });
            });

            context.history().on('change.intro', function(changed) {
                if (changed.created().length === 1) {
                    continueTo(continueDragMidpoint);
                }
            });

            context.on('enter.intro', function(mode) {
                if (mode.id !== 'select') {
                    // keep Wood Road selected so midpoint triangles are drawn..
                    context.enter(modeSelect(context, [midpointNodeRoadID]));
                }
            });
        }, msec + 100);

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.history().on('change.intro', null);
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function continueDragMidpoint() {
        if (!context.hasEntity(midpointNodeRoadID)) {
            return continueTo(updateLine);
        }

        var padding = 100 * Math.pow(2, context.map().zoom() - 19);
        var box = pad(midpointNode, padding, context);

        var advance = function() {
            context.history().checkpoint('doneUpdateLine');
            continueTo(deleteLines);
        };

        reveal(box, helpHtml('intro.tips.continue_drag_midpoint'),
            { buttonText: t.html('intro.ok'), buttonCallback: advance }
        );

        context.map().on('move.intro drawn.intro', function() {
            if (!context.hasEntity(midpointNodeRoadID)) {
                return continueTo(updateLine);
            }
            var padding = 100 * Math.pow(2, context.map().zoom() - 19);
            var box = pad(midpointNode, padding, context);
            reveal(box, helpHtml('intro.tips.continue_drag_midpoint'),
                { duration: 0, buttonText: t.html('intro.ok'), buttonCallback: advance }
            );
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            nextStep();
        }
    }


    function deleteLines() {
        context.history().reset('doneUpdateLine');
        context.enter(modeBrowse(context));

        if (!context.hasEntity(washingtonStreetID) ||
            !context.hasEntity(twelfthAvenueID) ||
            !context.hasEntity(eleventhAvenueEndID)) {
            return chapter.restart();
        }

        var msec = transitionTime(deleteLinesLoc, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(deleteLinesLoc, 19, msec);

        timeout(function() {
            var padding = 200 * Math.pow(2, context.map().zoom() - 19);
            var box = pad(deleteLinesLoc, padding, context);
            box.top -= 200;
            box.height += 400;
            var advance = function() { continueTo(rightClickIntersection); };

            reveal(box, helpHtml('intro.tips.delete_lines', { street: t('intro.graph.name.cours-des-lacs') }),
                { buttonText: t.html('intro.ok'), buttonCallback: advance }
            );

            context.map().on('move.intro drawn.intro', function() {
                var padding = 200 * Math.pow(2, context.map().zoom() - 19);
                var box = pad(deleteLinesLoc, padding, context);
                box.top -= 200;
                box.height += 400;
                reveal(box, helpHtml('intro.tips.delete_lines', { street: t('intro.graph.name.cours-des-lacs') }),
                    { duration: 0, buttonText: t.html('intro.ok'), buttonCallback: advance }
                );
            });

            context.history().on('change.intro', function() {
                timeout(function() {
                    continueTo(deleteLines);
                }, 500);  // after any transition (e.g. if user deleted intersection)
            });

        }, msec + 100);

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function rightClickIntersection() {
        context.history().reset('doneUpdateLine');
        context.enter(modeBrowse(context));

        context.map().centerZoomEase(eleventhAvenueEnd, 19, 500);

        var rightClickString = helpHtml('intro.tips.split_street', {
                street: t('intro.graph.name.cours-des-lacs')
            }) +
            helpHtml('intro.tips.' + (context.lastPointerType() === 'mouse' ? 'rightclick_intersection' : 'edit_menu_intersection_touch'));

        timeout(function() {
            var padding = 60 * Math.pow(2, context.map().zoom() - 19);
            var box = pad(eleventhAvenueEnd, padding, context);
            reveal(box, rightClickString);

            context.map().on('move.intro drawn.intro', function() {
                var padding = 60 * Math.pow(2, context.map().zoom() - 19);
                var box = pad(eleventhAvenueEnd, padding, context);
                reveal(box, rightClickString,
                    { duration: 0 }
                );
            });

            context.on('enter.intro', function(mode) {
                if (mode.id !== 'select') return;
                var ids = context.selectedIDs();
                if (ids.length !== 1 || ids[0] !== eleventhAvenueEndID) return;

                timeout(function() {
                    var node = selectMenuItem(context, 'split').node();
                    if (!node) return;
                    continueTo(splitIntersection);
                }, 50);  // after menu visible
            });

            context.history().on('change.intro', function() {
                timeout(function() {
                    continueTo(deleteLines);
                }, 300);  // after any transition (e.g. if user deleted intersection)
            });

        }, 600);

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.on('enter.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function splitIntersection() {
        if (!context.hasEntity(washingtonStreetID) ||
            !context.hasEntity(twelfthAvenueID) ||
            !context.hasEntity(eleventhAvenueEndID)) {
            return continueTo(deleteLines);
        }

        var node = selectMenuItem(context, 'split').node();
        if (!node) { return continueTo(rightClickIntersection); }

        var wasChanged = false;
        _washingtonSegmentID = null;

        reveal('.edit-menu', helpHtml('intro.tips.split_intersection',
            { street: t('intro.graph.name.cours-des-lacs') }),
            { padding: 50 }
        );

        context.map().on('move.intro drawn.intro', function() {
            var node = selectMenuItem(context, 'split').node();
            if (!wasChanged && !node) { return continueTo(rightClickIntersection); }

            reveal('.edit-menu', helpHtml('intro.tips.split_intersection',
                { street: t('intro.graph.name.cours-des-lacs') }),
                { duration: 0, padding: 50 }
            );
        });

        context.history().on('change.intro', function(changed) {
            wasChanged = true;
            timeout(function() {
                if (context.history().undoAnnotation() === t('operations.split.annotation.line', { n: 1 })) {
                    _washingtonSegmentID = changed.modified()[0].id;
                    continueTo(didSplit);
                } else {
                    _washingtonSegmentID = null;
                    continueTo(retrySplit);
                }
            }, 300);  // after any transition (e.g. if user deleted intersection)
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function retrySplit() {
        context.enter(modeBrowse(context));
        context.map().centerZoomEase(eleventhAvenueEnd, 18, 500);
        var advance = function() { continueTo(rightClickIntersection); };

        var padding = 60 * Math.pow(2, context.map().zoom() - 18);
        var box = pad(eleventhAvenueEnd, padding, context);
        reveal(box, helpHtml('intro.tips.retry_split'),
            { buttonText: t.html('intro.ok'), buttonCallback: advance }
        );

        context.map().on('move.intro drawn.intro', function() {
            var padding = 60 * Math.pow(2, context.map().zoom() - 18);
            var box = pad(eleventhAvenueEnd, padding, context);
            reveal(box, helpHtml('intro.tips.retry_split'),
                { duration: 0, buttonText: t.html('intro.ok'), buttonCallback: advance }
            );
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            nextStep();
        }
    }


    function didSplit() {
        if (!_washingtonSegmentID ||
            !context.hasEntity(_washingtonSegmentID) ||
            !context.hasEntity(washingtonStreetID) ||
            !context.hasEntity(twelfthAvenueID) ||
            !context.hasEntity(eleventhAvenueEndID)) {
            return continueTo(rightClickIntersection);
        }

        var ids = context.selectedIDs();
        var string = 'intro.tips.did_split_' + (ids.length > 1 ? 'multi' : 'single');
        var street = t('intro.graph.name.cours-des-lacs');

        var padding = 200 * Math.pow(2, context.map().zoom() - 19);
        var box = pad(twelfthAvenue, padding, context);
        box.height = box.height / 2;
        box.top = box.top + (box.height / 2);
        reveal(box, helpHtml(string, { street1: street, street2: street }),
            { duration: 500 }
        );

        timeout(function() {
            context.map().centerZoomEase(twelfthAvenue, 19, 500);

            context.map().on('move.intro drawn.intro', function() {
                var padding = 200 * Math.pow(2, context.map().zoom() - 19);
                var box = pad(twelfthAvenue, padding, context);
                box.height = box.height / 2;
                box.top = box.top + (box.height / 2);
                reveal(box, helpHtml(string, { street1: street, street2: street }),
                    { duration: 0 }
                );
            });
        }, 600);  // after initial reveal and curtain cut

        context.on('enter.intro', function() {
            var ids = context.selectedIDs();
            if (ids.length === 1 && ids[0] === _washingtonSegmentID) {
                continueTo(multiSelect);
            }
        });

        context.history().on('change.intro', function() {
            if (!_washingtonSegmentID ||
                !context.hasEntity(_washingtonSegmentID) ||
                !context.hasEntity(washingtonStreetID) ||
                !context.hasEntity(twelfthAvenueID) ||
                !context.hasEntity(eleventhAvenueEndID)) {
                return continueTo(rightClickIntersection);
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.on('enter.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function multiSelect() {
        if (!_washingtonSegmentID ||
            !context.hasEntity(_washingtonSegmentID) ||
            !context.hasEntity(washingtonStreetID) ||
            !context.hasEntity(twelfthAvenueID) ||
            !context.hasEntity(eleventhAvenueEndID)) {
            return continueTo(rightClickIntersection);
        }

        var ids = context.selectedIDs();
        var hasWashington = ids.indexOf(_washingtonSegmentID) !== -1;
        var hasTwelfth = ids.indexOf(twelfthAvenueID) !== -1;

        if (hasWashington && hasTwelfth) {
            return continueTo(multiRightClick);
        } else if (!hasWashington && !hasTwelfth) {
            return continueTo(didSplit);
        }

        context.map().centerZoomEase(twelfthAvenue, 19, 500);

        timeout(function() {
            var selected, other, padding, box;
            if (hasWashington) {
                selected = t('intro.graph.name.cours-des-lacs');
                other = t('intro.graph.name.chemin-des-lacs');
                padding = 60 * Math.pow(2, context.map().zoom() - 19);
                box = pad(twelfthAvenueEnd, padding, context);
                box.height = box.height * 2;
            } else {
                selected = t('intro.graph.name.chemin-des-lacs');
                other = t('intro.graph.name.cours-des-lacs');
                padding = 200 * Math.pow(2, context.map().zoom() - 19);
                box = pad(twelfthAvenue, padding, context);
                box.height = box.height * 2/3;
                box.top = box.top + box.height / 6;
            }

            reveal(box,
                helpHtml('intro.tips.multi_select',
                    { selected: selected, other1: other }) + ' ' +
                helpHtml('intro.tips.add_to_selection_' + (context.lastPointerType() === 'mouse' ? 'click' : 'touch'),
                    { selected: selected, other2: other })
            );

            context.map().on('move.intro drawn.intro', function() {
                if (hasWashington) {
                    selected = t('intro.graph.name.cours-des-lacs');
                    other = t('intro.graph.name.chemin-des-lacs');
                    padding = 60 * Math.pow(2, context.map().zoom() - 19);
                    box = pad(twelfthAvenueEnd, padding, context);
                    box.height = box.height * 2;
                } else {
                    selected = t('intro.graph.name.chemin-des-lacs');
                    other = t('intro.graph.name.cours-des-lacs');
                    padding = 200 * Math.pow(2, context.map().zoom() - 19);
                    box = pad(twelfthAvenue, padding, context);
                    box.height = box.height * 2/3;
                    box.top = box.top + box.height / 6;
                }

                reveal(box,
                    helpHtml('intro.tips.multi_select',
                        { selected: selected, other1: other }) + ' ' +
                    helpHtml('intro.tips.add_to_selection_' + (context.lastPointerType() === 'mouse' ? 'click' : 'touch'),
                        { selected: selected, other2: other }),
                    { duration: 0 }
                );
            });

            context.on('enter.intro', function() {
                continueTo(multiSelect);
            });

            context.history().on('change.intro', function() {
                if (!_washingtonSegmentID ||
                    !context.hasEntity(_washingtonSegmentID) ||
                    !context.hasEntity(washingtonStreetID) ||
                    !context.hasEntity(twelfthAvenueID) ||
                    !context.hasEntity(eleventhAvenueEndID)) {
                    return continueTo(rightClickIntersection);
                }
            });
        }, 600);

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.on('enter.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function multiRightClick() {
        if (!_washingtonSegmentID ||
            !context.hasEntity(_washingtonSegmentID) ||
            !context.hasEntity(washingtonStreetID) ||
            !context.hasEntity(twelfthAvenueID) ||
            !context.hasEntity(eleventhAvenueEndID)) {
            return continueTo(rightClickIntersection);
        }

        var padding = 200 * Math.pow(2, context.map().zoom() - 19);
        var box = pad(twelfthAvenue, padding, context);
        var rightClickUpperLoc = [2.6493131, 48.8391915];
        context.map().centerZoomEase(rightClickUpperLoc, 19, 500);

        var rightClickString = helpHtml('intro.tips.multi_select_success') +
            helpHtml('intro.tips.multi_' + (context.lastPointerType() === 'mouse' ? 'rightclick' : 'edit_menu_touch'));
        reveal(box, rightClickString);

        context.map().on('move.intro drawn.intro', function() {
            var padding = 200 * Math.pow(2, context.map().zoom() - 19);
            var box = pad(twelfthAvenue, padding, context);
            reveal(box, rightClickString, { duration: 0 });
        });

        context.ui().editMenu().on('toggled.intro', function(open) {
            if (!open) return;

            timeout(function() {
                var ids = context.selectedIDs();
                if (ids.length === 2 &&
                    ids.indexOf(twelfthAvenueID) !== -1 &&
                    ids.indexOf(_washingtonSegmentID) !== -1) {
                        var node = selectMenuItem(context, 'delete').node();
                        if (!node) return;
                        continueTo(multiDelete);
                } else if (ids.length === 1 &&
                    ids.indexOf(_washingtonSegmentID) !== -1) {
                    return continueTo(multiSelect);
                } else {
                    return continueTo(didSplit);
                }
            }, 300);  // after edit menu visible
        });

        context.history().on('change.intro', function() {
            if (!_washingtonSegmentID ||
                !context.hasEntity(_washingtonSegmentID) ||
                !context.hasEntity(washingtonStreetID) ||
                !context.hasEntity(twelfthAvenueID) ||
                !context.hasEntity(eleventhAvenueEndID)) {
                return continueTo(rightClickIntersection);
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.ui().editMenu().on('toggled.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function multiDelete() {
        if (!_washingtonSegmentID ||
            !context.hasEntity(_washingtonSegmentID) ||
            !context.hasEntity(washingtonStreetID) ||
            !context.hasEntity(twelfthAvenueID) ||
            !context.hasEntity(eleventhAvenueEndID)) {
            return continueTo(rightClickIntersection);
        }

        var node = selectMenuItem(context, 'delete').node();
        if (!node) return continueTo(multiRightClick);

        reveal('.edit-menu',
            helpHtml('intro.tips.multi_delete'),
            { padding: 50 }
        );

        context.map().on('move.intro drawn.intro', function() {
            reveal('.edit-menu',
                helpHtml('intro.tips.multi_delete'),
                { duration: 0, padding: 50 }
            );
        });

        context.on('exit.intro', function() {
            if (context.hasEntity(_washingtonSegmentID) || context.hasEntity(twelfthAvenueID)) {
                return continueTo(multiSelect);  // left select mode but roads still exist
            }
        });

        context.history().on('change.intro', function() {
            if (context.hasEntity(_washingtonSegmentID) || context.hasEntity(twelfthAvenueID)) {
                continueTo(retryDelete);         // changed something but roads still exist
            } else {
                continueTo(endDelete);
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.on('exit.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function retryDelete() {
        context.enter(modeBrowse(context));

        var padding = 200 * Math.pow(2, context.map().zoom() - 18);
        var box = pad(twelfthAvenue, padding, context);
        reveal(box, helpHtml('intro.tips.retry_delete'), {
            buttonText: t.html('intro.ok'),
            buttonCallback: function() { continueTo(multiSelect); }
        });

        function continueTo(nextStep) {
            nextStep();
        }
    }


    function endDelete() {
        var padding = 200 * Math.pow(2, context.map().zoom() - 19);
        var box = pad(twelfthAvenue, padding, context);
        var advance = function() { continueTo(searchHospital); };

        reveal(box, helpHtml('intro.tips.end_delete'),
            { buttonText: t.html('intro.ok'), buttonCallback: advance }
        );

        context.map().on('move.intro drawn.intro', function() {
            var padding = 200 * Math.pow(2, context.map().zoom() - 19);
            var box = pad(twelfthAvenue, padding, context);
            reveal(box, helpHtml('intro.tips.end_delete'),
                { duration: 0, buttonText: t.html('intro.ok'), buttonCallback: advance }
            );
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            nextStep();
        }
    }


    function searchHospital() {
        context.enter(modeBrowse(context));
        context.history().reset('initial');  // ensure spring street exists

        var msec = transitionTime(hospital, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(hospital, 19, msec);  // ..and user can see it

        timeout(function() {
            reveal('.search-header input',
                helpHtml('intro.tips.search_hospital', { name: t('intro.graph.name.centre-medical-occident') })
            );

            context.container().select('.search-header input')
                .on('keyup.intro', checkSearchResult);
        }, msec + 100);
    }


    function checkSearchResult() {
        var first = context.container().select('.feature-list-item:nth-child(0n+2)');  // skip "No Results" item
        var firstName = first.select('.entity-name');
        var name = t('intro.graph.name.centre-medical-occident');

        if (!firstName.empty() && firstName.html() === name) {
            reveal(first.node(),
                helpHtml('intro.tips.choose_hospital', { name: name }),
                { duration: 300 }
            );

            context.on('exit.intro', function() {
                continueTo(selectedHospital);
            });

            context.container().select('.search-header input')
                .on('keydown.intro', eventCancel, true)
                .on('keyup.intro', null);
        }

        function continueTo(nextStep) {
            context.on('exit.intro', null);
            context.container().select('.search-header input')
                .on('keydown.intro', null)
                .on('keyup.intro', null);
            nextStep();
        }
    }


    function selectedHospital() {
        if (!context.hasEntity(hospitalID)) {
            return searchHospital();
        }

        var onClick = function() { continueTo(rightClickHospital); };
        var entity = context.entity(hospitalID);

        revealHospital(
            hospital,
            helpHtml('intro.tips.selected_hospital', { name: t('intro.graph.name.centre-medical-occident') }),
            { duration: 300, buttonText: t.html('intro.ok'), buttonCallback: onClick }
        );

        context.on('enter.intro', function(mode) {
            if (!context.hasEntity(hospitalID)) {
                return continueTo(searchHospital);
            }
            var ids = context.selectedIDs();
            if (mode.id !== 'select' || !ids.length || ids[0] !== hospitalID) {
                // keep Spring Street selected..
                context.enter(modeSelect(context, [hospitalID]));
            }
        });

        context.history().on('change.intro', function() {
            if (!context.hasEntity(hospitalID)) {
                timeout(function() {
                    continueTo(searchHospital);
                }, 300);  // after any transition (e.g. if user deleted intersection)
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            nextStep();
        }
    }


    function revealHospital(center, text, options) {
        var padding = 160 * Math.pow(2, context.map().zoom() - 20);
        var box = pad(center, padding, context);
        reveal(box, text, options);
    }


    function rightClickHospital() {
        if (!context.hasEntity(hospitalID)) { return searchHospital(); }

        context.on('enter.intro', function(mode) {
            if (mode.id !== 'select') return;
            var ids = context.selectedIDs();
            if (ids.length !== 1 || ids[0] !== hospitalID) return;

            timeout(function() {
                var node = selectMenuItem(context, 'orthogonalize').node();
                if (!node) return;
                continueTo(clickSquare);
            }, 50);  // after menu visible
        });

        var rightclickString = helpHtml('intro.tips.' + (context.lastPointerType() === 'mouse' ? 'rightclick_building' : 'edit_menu_building_touch'));
        revealHospital(hospital, rightclickString, { duration: 600 });

        context.history().on('change.intro', function() {
            continueTo(rightClickHospital);
        });

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            context.map().on('move.intro drawn.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function clickSquare() {
        if (!hospitalID) return chapter.restart();
        var entity = context.hasEntity(hospitalID);
        if (!entity) return continueTo(rightClickHospital);

        var node = selectMenuItem(context, 'orthogonalize').node();
        if (!node) { return continueTo(rightClickHospital); }

        var wasChanged = false;

        reveal('.edit-menu',
            helpHtml('intro.tips.square_building'),
            { padding: 50 }
        );

        context.on('enter.intro', function(mode) {
            if (mode.id === 'browse') {
                continueTo(rightClickHospital);
            } else if (mode.id === 'move' || mode.id === 'rotate') {
                continueTo(retryClickSquare);
            }
        });

        context.map().on('move.intro', function() {
            var node = selectMenuItem(context, 'orthogonalize').node();
            if (!wasChanged && !node) { return continueTo(rightClickHospital); }

            reveal('.edit-menu',
                helpHtml('intro.tips.square_building'),
                { duration: 0, padding: 50 }
            );
        });

        context.history().on('change.intro', function() {
            wasChanged = true;
            context.history().on('change.intro', null);

            // Something changed.  Wait for transition to complete and check undo annotation.
            timeout(function() {
                if (context.history().undoAnnotation() === t('operations.orthogonalize.annotation.feature', { n: 1 })) {
                    continueTo(doneSquare);
                } else {
                    continueTo(retryClickSquare);
                }
            }, 500);  // after transitioned actions
        });

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            context.map().on('move.intro', null);
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function retryClickSquare() {
        context.enter(modeBrowse(context));

        revealHospital(hospital, helpHtml('intro.buildings.retry_square'), {
            buttonText: t.html('intro.ok'),
            buttonCallback: function() { continueTo(rightClickHospital); }
        });

        function continueTo(nextStep) {
            nextStep();
        }
    }


    function doneSquare() {
        context.history().checkpoint('doneSquare');

        revealHospital(hospital, helpHtml('intro.tips.done_square'), {
            buttonText: t.html('intro.ok'),
            buttonCallback: function() { continueTo(play); }
        });

        function continueTo(nextStep) {
            nextStep();
        }
    }


    function play() {
        dispatch.call('done');
        reveal('.ideditor',
            helpHtml('intro.tips.play', { next: t('intro.startediting.title') }), {
                tooltipBox: '.intro-nav-wrap .chapter-startEditing',
                buttonText: t.html('intro.ok'),
                buttonCallback: function() { reveal('.ideditor'); }
            }
        );
    }


    chapter.enter = function() {
        updateLine();
    };


    chapter.exit = function() {
        timeouts.forEach(window.clearTimeout);
        context.on('enter.intro exit.intro', null);
        context.map().on('move.intro drawn.intro', null);
        context.history().on('change.intro', null);
        context.container().select('.inspector-wrap').on('wheel.intro', null);
        context.container().select('.preset-search-input').on('keydown.intro keyup.intro', null);
        context.container().select('.more-fields .combobox-input').on('click.intro', null);
    };


    chapter.restart = function() {
        chapter.exit();
        chapter.enter();
    };


    return utilRebind(chapter, dispatch, 'on');
}
