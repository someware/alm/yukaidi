import { osmApiConnections } from '../config/id.js';
import { OidcAuth } from './services/oidc';


var redirectPath = DEPLOY_URL;

var oauth = new OidcAuth({
  authority: OIDC_SERVER,
  client_id: OAUTH_CLIENT_ID,
  redirect_uri: redirectPath + 'land.html',
  post_logout_redirect_uri: redirectPath + 'index.html',
  scope: 'openid profile email',
}, osmApiConnections[0].url);

export default oauth;
