import store from 'store';
import { UserManager } from "oidc-client-ts";


export class OidcAuth {
  constructor(settings, api_url) {
    this.api_url = api_url;
    this.userManager = new UserManager(settings);
  }

  async authenticated() {
    return await this.userManager.getUser() !== null;
  }

  async logout() {
    if (await this.authenticated()) {
      this.userManager.signoutRedirect();
    }
    await this.userManager.removeUser();
    return this;
  }

  signinCallback() {
    const self = this;
    this.userManager.signinRedirectCallback().then(function(user) {
      self.userManager.storeUser(user);
      window.location.href=DEPLOY_URL+'index.html';
      console.log("signed in", user);
    }).catch(function(err) {
      console.error(err);
      console.log(err);
    });
  }

  async authenticate(callback) {
    if (await this.authenticated()) {
      callback(null, this);
      return;
    }

    await this.userManager.removeUser();

    try {
      await this.userManager.signinRedirect();
      callback(null, this);
    } catch (e) {
      console.log(e);
    }
  }

  async xhr(options, callback) {
    const self = this;

    if (await this.authenticated()) {
      return run();
    } else {
      callback('not authenticated', null);
    }

    async function run() {
      var url = self.api_url + options.path;
      var user = await self.userManager.getUser();
      var token = user?.access_token;
      return self.rawxhr(
        options.method,
        url,
        token,
        options.content,
        options.headers,
        done
      );

    }

    function done(err, xhr) {
      if (err) {
        callback(err);
      } else if (xhr.responseXML) {
        callback(err, xhr.responseXML);
      } else {
        callback(err, xhr.response);
      }
    }
  }

  rawxhr(method, url, access_token, data, headers, callback) {
    headers = headers || { 'Content-Type': 'application/x-www-form-urlencoded' };

    if (access_token) {
      headers.Authorization = 'Bearer ' + access_token;
    }

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
      if (4 === xhr.readyState && 0 !== xhr.status) {
        if (/^20\d$/.test(xhr.status)) {   // a 20x status code - OK
          callback(null, xhr);
        } else {
          callback(xhr, null);
        }
      }
    };
    xhr.onerror = function (e) {
      callback(e, null);
    };

    xhr.open(method, url, true);
    for (var h in headers) xhr.setRequestHeader(h, headers[h]);

    xhr.send(data);
    return xhr;
  }

}
